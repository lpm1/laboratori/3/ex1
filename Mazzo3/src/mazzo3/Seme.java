/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mazzo3;

/**
 *
 * @author giovanni
 */
public class Seme {

    int numero;

    public Seme(int numero) {
        if (numero >= 0 && numero <= 3) {
            this.numero = numero;
        } else {
            this.numero = -1;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.numero;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Seme other = (Seme) obj;
        return this.numero == other.numero;
    }

    @Override
    public String toString() {
        String res = "";
        switch (this.numero) {
            case 0:
                res += "C";
                break;
            case 1:
                res += "Q";
                break;
            case 2:
                res += "F";
                break;
            case 3:
                res += "P";
                break;
        }
        return res;
    }
}
