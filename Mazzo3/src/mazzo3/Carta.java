/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mazzo3;

import java.util.Objects;

/**
 *
 * @author giovanni
 */
public class Carta {

    int numero;
    Seme seme;

    public Carta(int numero, Seme seme) {
        this.numero = numero;
        this.seme = seme;
    }

    public Carta() {
        this(0, new Seme(0));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.numero;
        hash = 53 * hash + Objects.hashCode(this.seme);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carta other = (Carta) obj;
        if (this.numero != other.numero) {
            return false;
        }
        return Objects.equals(this.seme, other.seme);
    }

    public String numeroToString(){
        String res ="";
        switch(this.numero){
            case 0:
                res+="A";
                break;
            case 10:
                res+="J";
                break;
            case 11:
                res+="Q";
                break;
            case 12:
                res+="K";
                break;
            default:
                res+=this.numero; 
        }
        return res;
    }
    
    @Override
    public String toString() {
        String res = "";
        res += this.numeroToString();
        res += this.seme;
        return res;
    }

}
